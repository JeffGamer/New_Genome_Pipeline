#!/bin/bash
#SBATCH --job-name=TSA_frameselect
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=esgrau@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

#run this in the main TSA folder (?)
#run frame select
for f in /isg/treegenes/treegenes_store/FTP/Transcriptome/TSA/*/; do
    species=$(basename "$f" )
    echo $species
    cd $species
    mkdir .frame_select
    mkdir .frame_yikes

#if running from transcriptome use path /isg/treegenes/treegenes_store/FTP/Transcriptome/TSA/$item/*.fasta
    cp ./*.fasta ./.frame_yikes

    cd .frame_yikes
    echo $(pwd)

    module load hmmer/3.1b2
    module load perl/5.24.0

    module load TransDecoder/5.3.0

    #frame select - transdecoder, train with the #longest sequences
    TransDecoder.LongOrfs -t *.fasta
    #predict the remaining ORF
    TransDecoder.Predict -t *.fasta --single_best_only

    cd ../.frame_select

    cp ../.frame_yikes/*.cds ./
    cp ../.frame_yikes/*.pep ./

    #rm -r ../.frame_yikes

#run rnaquast
        module load rnaQUAST/1.5.2
        module load GeneMarkS-T/5.1

        rnaQUAST.py --transcripts $species"_TSA.fasta.transdecoder.cds" --gene_mark --threads 8 -o ./results

#run BUSCO
    module load busco/4.0.2
    module unload augustus
    export PATH=/home/CAM/ssarker/augustus/3.2.3/bin:/home/CAM/ssarker/augustus/3.2.3/scripts:$PATH
    export AUGUSTUS_CONFIG_PATH=$HOME/augustus/3.2.3/config

    run_BUSCO.py -i *.pep -l /isg/shared/databases/BUSCO/odb10/embryophyta_odb10/ -o busco_out -m proteins -c 8

#make read me with results
        (echo "########"; echo "rnaQUAST Results"; echo "########"; echo "rnaQUAST version is: 1.5.2"; echo "" ;cat ./results/short_report.txt; echo ""; echo "########"; echo "BUSCO Results"; echo "########"; echo "" ; cat ./run_busco_out/short_summary_busco_out.txt) > README.txt

#cleanup
    rm -r results
    rm -r run_busco_out
    rm -r tmp


    mkdir index
    cd index
    # diamond for genome
    echo $species"_TSA.fasta.transdecoder.pep" 
    module load diamond
    #Wono_TSA.fasta.transdecoder.pep
    diamond makedb --in ../$species"_TSA.fasta.transdecoder.pep" -d $species

    echo $(pwd)
    cd ../..
    mkdir ../../.TSA_Frame_Selected/$species
    cp -R .frame_select/* ../../.TSA_Frame_Selected/$species/
    cd ..
done

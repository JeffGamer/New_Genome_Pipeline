#!/bin/bash
#SBATCH --job-name=genome_index
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 12
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=esgrau@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

##Note: more memory/cores and a different partition (xeon) may be required for larger genomes like conifers.

#run insdide genome version folder
if [ "$#" -ne 3 ]; then
    echo "Incorrect number of arguments"
    echo "Usage: generateIndex.sh target_nucletotide.fa  gtf_file.gtf speciesCode"
    echo "Usage example: sbatch hisat2_annotation.sh Eugr.1_0.fa Eugr.1_0.gtf Eugr.1_0"
#code = 4letter code + version ex= Bena.1_0
else
    fasta=$1
    code=$3
    gtf=$2

    mkdir index  index/genome_index

#genome index folder

    #create splice site and exon files
    module load python/3.6.3
    python hisat2_extract_splice_sites.py $gtf > "${code}.splicesites.txt"
    python hisat2_extract_exons.py $gtf > "${code}.exons.txt"
    # hisat2
    module load hisat2
    mkdir index/genome_index/"${code}.hisat2"
    hisat2-build --large-index $fasta -p 12 --ss "${code}.splicesites.txt" --exon "${code}.exons.txt" index/genome_index/"${code}.hisat2"/"${code}.hisat2"

    #zip genome fasta back up
    gzip ./genome/*.fa
    
fi

#!/bin/bash
#SBATCH --job-name=tsa_index
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sunny.sarker@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

# Usage: genome_indexer.sh directoryName target_nucletotide.fa target_protein.fa speciesCode
#        Will create a directory for the .fa file containing a directory for
#        each type of index
#        Run in the /index/ level of the genome directory
# Example:
#GOAL use a loop script to for item in */
#sbatch *.fasta *.pep $item 
#    sh generateIndex.sh Egrandis_162_cds.fa Egrandis_162_pep.fa EUGR

#run insdide genome version folder
if [ "$#" -ne 3 ]; then
    echo "Incorrect number of arguments"
    echo "Usage: generateIndex.sh target_nucletotide.fa target_protein.fa speciesCode"
    echo "Usage example: sh generateIndex.sh Egrandis_162_cds.fa Egrandis_162_pep.fa EUGR"

else
    fasta=$1
    pfasta=$2
    code=$3

    mkdir index index/"${code}.tsa.protein_diamond" index/"${code}.tsa.gene_blast"

    #protein blast + diamond
    #diamond
    #requires PROTEIN fasta
    
    module load diamond
    diamond makedb --in $pfasta -d $code
    mv $code.dmnd ./index/"${code}.tsa.protein_diamond"

    # blast for protein
    
    module load blast
    makeblastdb -in $fasta -parse_seqids -dbtype prot -out index/"${code}.tsa.gene_blast"/"${code}.tsa.gene_blast"

fi

#can make quick with a for loop inside FTP/Transcriptome/TSA
#for item in */
#do
#	cd $item
#
 #   folder_name = "${item}"
#    sbatch /home/CAM/ssarker/tsa_work/TSA/genome_indexer.sh *.fasta *.pep "$folder_name"
#
#	cd ..
#
#done

#!/bin/bash
#SBATCH --job-name=genome_index
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=esgrau@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

# Usage: genome_indexer.sh directoryName target_nucletotide.fa target_protein.fa speciesCode
# use a loop script to for item in */
#sbatch *.fasta *.pep $item
#    sh generateIndex.sh Egrandis_162_cds.fa Egrandis_162_pep.fa EUGR
#run in base version of folder fasta = ./genome/*.fa

#run insdide genome version folder
if [ "$#" -ne 2 ]; then
    echo "Incorrect number of arguments"
    echo "Usage: generateIndex.sh target_nucletotide.fa  speciesCode"
    echo "Usage example: sh generateIndex.sh Egrandis_162_cds.fa EUGR"
#code = 4letter code + version ex= Bena.1_0
else
    fasta=$1
    code=$2

    mkdir ../index  ../index/genome_index

#genome index folder
    # blast for genome
    module load blast
    makeblastdb -in $fasta -parse_seqids -dbtype nucl -out ../index/genome_index/"${code}.genome_blast"/"${code}.genome_blast"

    # bwa
    module load bwa
    mkdir ../index/genome_index/"${code}.bwa"
    bwa index -p ../index/genome_index/"${code}.bwa"/"${code}.bwa" $fasta

    # gmap section (for building index NOT aligning)
    module load gmap/2019-06-10
    gmap_build -D ../index/genome_index -d "${code}.gmap" $fasta

    # hisat2
    module load hisat2
    mkdir ../index/genome_index/"${code}.hisat2"
    hisat2-build --large-index $fasta -p 12 ../index/genome_index/"${code}.hisat2"/"${code}.hisat2"



fi


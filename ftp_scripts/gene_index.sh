#!/bin/bash
#SBATCH --job-name=gene_index
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=esgrau@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

if [ "$#" -ne 2 ]; then
    echo "Incorrect number of arguments"
    echo "Usage: generateIndex.sh target_cds.fa  speciesCode"
    echo "Usage example: sh gene_index_folder.sh Egrandis_162_cds.fa EUGR.1_0"
#code = 4letter code + version ex= Bena.1_0

else
    fasta=$1 #this will be the CDS fasta
    code=$2

    mkdir index index/gene_index index/genome_index index/protein_index


#genome index folder
    # blast 
    module load blast
    makeblastdb -in $fasta -parse_seqids -dbtype nucl -out index/gene_index/"${code}.gene_blast"/"${code}.gene_blast"


    gzip ./annotation/"${code}.cds.fa"
fi
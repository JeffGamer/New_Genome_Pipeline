#!/bin/bash
#SBATCH --job-name=annotation_stats
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=80G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=esgrau@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

if [ "$#" -ne 2 ]; then
    echo "Incorrect number of arguments"

#run in main genome folder version
else
    cfasta=$1 #cds fasta
    pfasta=$2 #this will be the pep fasta
    
    #run rnaquast
    module load rnaQUAST/1.5.2
    module load GeneMarkS-T/5.1
    
    rnaQUAST.py --transcripts $cfasta --gene_mark --threads 8 -o ./results

	#run BUSCO
	
	module load busco/4.0.2 
	module unload augustus
  	export PATH=/home/CAM/ssarker/augustus/3.2.3/bin:/home/CAM/ssarker/augustus/3.2.3/scripts:$PATH
  	export AUGUSTUS_CONFIG_PATH=$HOME/augustus/3.2.3/config

  	run_BUSCO.py -i $pfasta -l /isg/shared/databases/BUSCO/odb10/embryophyta_odb10/ -o busco_out -m proteins -c 1

	#make read me with results

(echo "########"; echo "rnaQUAST Results"; echo "########"; echo "" ;cat ./results/short_report.txt; echo ""; echo "########"; echo "BUSCO Results"; echo "########"; echo "" ;cat ./busco_out/short_summary.specific.embryophyta_odb10.busco_out.txt) > ./annotation_README.txt


	#cleanup
	rm -r results
  	rm -r *busco*
  	rm -r tmp
  	rm -r quast*
fi

#!/bin/bash
#SBATCH --job-name=genome_folder
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=30G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=esgrau@gmail.com
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err

#run this script inside the genomes main folder/version

cd genome/

#run quast
module load quast/5.0.2

quast.py *.fa -o quast_out

#run BUSCO

module load busco/3.0.2b
module unload augustus
export PATH=/home/CAM/ssarker/augustus/3.2.3/bin:/home/CAM/ssarker/augustus/3.2.3/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/augustus/3.2.3/config

run_BUSCO.py -i *.fa -l /isg/shared/databases/BUSCO/odb10/embryophyta_odb10/ -o busco_out -m geno -c 1

#make read me with results
sed '/^# To reproduce this run:/d' < ./run_busco_out/short_summary_busco_out.txt > ./run_busco_out/busco_readme.txt

(cat ./quast_out/report.txt; echo "########"; echo "Quast Results"; echo "########"; cat ./run_busco_out/busco_readme.txt) > README.txt

#the read me will contain the results and be located in the main genome folder

#delete the other directories
rm -r quast_out
rm -r busco_out
rm -r tmp
#!/bin/bash
#SBATCH --job-name=prot_genome_index
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sunny.sarker@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

if [ "$#" -ne 2 ]; then
    echo "Incorrect number of arguments"
    echo "Usage: generateIndex.sh target_nucletotide.fa  speciesCode"
    echo "Usage example: sh generateIndex.sh Egrandis_162_pep.fa EUGR"
#run in main genome folder version
else
    fasta=$1 #this will be the pep fasta
    code=$2 #code = 4letter code + version ex= Bena.1_0

    mkdir index index/gene_index index/genome_index index/protein_index


#genome index folder
    # blast for genome
    
    module load blast
    makeblastdb -in $fasta -parse_seqids -dbtype prot -out index/protein_index/"${code}.protein_blast"/"${code}.protein_blast"

    # diamond for genome
    
    module load diamond
    diamond makedb --in $fasta -d $code
    mkdir ./index/protein_index/"${code}.protein_diamond"
    mv $code.dmnd ./index/protein_index/"${code}.protein_diamond"/"${code}.protein_diamond"

fi

TSA_Frame_Selected structure: 
```
Qulo
│
├── Qulo_TSA.fasta - TSA fasta file from TSA folder
├── Qulo_TSA.fasta.transdecoder.cds - CDS file output from Transdecoder
├── Qulo_TSA.fasta.transdecoder.pep - peptide file output from Transdecoder
├── index
│    └── Qulo.tsa.protein_diamond
│         └── Qulo.dmd - DIAMOND index made with Transdecoder pep file (Run with whatever the latest version of DIAMOND is on Xanadu)
└──  README.txt - statistics from rnaQUAST (on Transdecoder CDS file) and BUSCO (on Transdecoder pep file)
    
```

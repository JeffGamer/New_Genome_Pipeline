#!/bin/bash
#SBATCH --job-name=gmapSaNe
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -o gmapSaNe_%j.o
#SBATCH -e gmapSaNe_%j.e
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=20G
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sunny.sarker@uconn.edu
#SBATCH -o %x_%A.out
#SBATCH -c 16
#SBATCH -e %x_%A.err

#this script runs both gmap and gfacs on the output of gmap

if [ "$#" -ne 3 ]; then
    echo "Incorrect number of arguments"
    echo "Usage: gmap_r.sh fasta.fa transcriptome.fa speciesCode"
    echo "Usage example: sh gmap_r.sh ./genome/Abal.fa ./TSA_framselect/Abal.fa Abal.v1_0 "
else
    fasta= $1 #path to genome fasta
    tsa=$2 #path to tsa_fram transrcipt fasta file
    code=$3 #Abal.1_0

    #run in main genome folder version

    module load gmap/2019-06-10

    mkdir alignments

    ''' if didnt run gmap_build for index already
    #run gmap build to create indexes of the whole genome fasta
    gmap_build -D ./index/genome_index -d "${code}".gmap $fasta
    '''

    #run gmap on the transcriptome file with the results of gmap_build
    gmap -D ./index/genome_index -d "${code}".gmap -f gff3_gene $tsa --nthreads=16 --min-intronlength=9 --min-trimmed-coverage=0.95 --min-identity=0.95 -n1 > "${code}".gmap.gff3 2> "${code}".gmap.error

    mv *.gff3 *.error ./alignments


    module load perl/5.24.0

    perl /labs/Wegrzyn/gFACs/gFACs.pl -f gmap_2017_03_17_gff3 --statistics --unique-genes-only --fasta $fasta -O ./"${code}.gfacs_output"/ ./alignments/*.gff3
    mv ./"${code}.gfacs_output"/statistics.txt ./alignments/README.txt
    mv ./"${code}.gfacs_output" ./alignments/

    gzip ./genome/*.fa

fi

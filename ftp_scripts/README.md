# FTP Documentation

## Table of Contents
1. [Introduction](#intro)
2. [Workflow Structure](#next)
3. [Genome Folder](#genome)
4. [Index Folder](#index)
   - [Genome Index](#genome_index)
   - [Hisat2 Index with Annotation](#hisat2_index_with_annotation)
   - [Gene Index](#gene_index)
   - [Protein Index](#prot)
5. [Annotation Folder](#ann)
   - [EnTAP](#en)
   - [rnaQUAST and BUSCO](#rna)
6. [Alignments Folder](#ali)
   - [GMAP and gFACs](#gmap)
7. [Plastid Folder](#plastid)

<a name="intro"></a>
## Introduction
This is the reference documentation for the <a href="https://treegenesdb.org/FTP/">Treegenes FTP</a>. This is publicly available for users to download our available genomic data on alignments, genome, annotation, and index data. In order to manage this data we have developed the following workflow in order to run the programs needed such as
[BUSCO](https://busco.ezlab.org/busco_userguide.html#outputs), [QUAST](http://quast.sourceforge.net/docs/manual.html#sec3), [BLAST](https://www.ncbi.nlm.nih.gov/books/NBK153387/), [Hisat2](https://ccb.jhu.edu/software/hisat2/manual.shtml#introduction), [Diamond](http://gensoft.pasteur.fr/docs/diamond/0.8.29/diamond_manual.pdf), [bwa](http://bio-bwa.sourceforge.net/bwa.shtml#1), [GMAP](https://omictools.com/gmap-tool
), [EnTAP](https://entap.readthedocs.io/en/latest/introduction.html) and [gFACs](https://gfacs.readthedocs.io/en/latest/Getting_Started/About.html).

Each genome folder within the FTP has the following structure and naming convention:

```
Qulo
│
└── v1.0
    ├──README.txt (optional) - Contains information about the genome project including publication/attribution. If genome was size selected and/or includes masked and unmasked scaffolds it will be noted here.
    ├──alignments (optional)
    │    ├── Qulo.1_0.gmap.gff
    │    ├── Qulo.1_0.tsa.fa - transcript fasta used for gmap run
    │    └── README.txt - summary of gmap results from gFACS
    ├──annotation
    │    ├── Qulo.1_0.gff.gz
    │    ├── Qulo.1_0.pep.fa.gz
    │    ├── Qulo.1_0.entap_annotations.tsv
    │    ├── Qulo.1_0.entap_statistics.txt
    │    ├── Qulo.1_0.repeats.fa.gz (optional)
    │    ├── Qulo.1_0.repeats.gff.gz (optional)

    │    └── README.txt - rnaQUAST and BUSCO results
    ├── genome
    │    ├── Qulo.1_0.fa.gz
    │    └── README.txt - QUAST and BUSCO results
    ├──  index
    │    ├── gene_index
    │    |    └── Qulo.1_0.gene_blast
    │    ├── genome_index
    │    |    ├── Qulo.1_0.bwa
    │    |    ├── Qulo.1_0.genome_blast
    │    |    ├── Qulo.1_0.gmap
    │    |    ├── Qulo.1_0.hisat2
    │    |    └── Qulo.1_0.hisat2_annotation
    │    └── protein_index
    │         ├── Qulo.1_0.protein_blast
    │         └── Qulo.1_0.protein_diamond
    ├── plastid (optional)
    │   ├── mitochondrial
    │   │    ├── Qulo.1_0.mito.gff.gz
    │   │    ├── Qulo.1_0.mito.fa.gz
    │   │    └── README.txt 
    │   └── chloroplast
    │        ├── Qulo.1_0.chloro.gff.gz
    │        ├── Qulo.1_0.chloro.fa.gz
    │        └── README.txt
    └──  supplemental (optional)
        ├── Qulo.1_0.annotations.tsv.gz (If original project provides annotation files they will be located in the supplemental directory. In-house annotations are located in the annotation directory.)
        ├── Qulo.1_0.snps.vcf.gz 
        └── Qulo.1_0.polymorphisms.vcf.gz
```

In this case the genome folder is "Qulo" which is the abbreviation for the Quercus loata (Valley oak) genome. The names of every genome is conatined in the following [README.txt](https://treegenesdb.org/FTP/Genomes/README.txt). 
Each genome folder has a/multiple version folders in which the latest genome folders would have more recent assemblies of the genome. Then within the genome version folder it is seperated into 4 different parts:

1. alignments folder
2. annnotaiton folder
3. genome folder
4. index folder

Within these folders every program run will output their files with the naming convention where it will start with the four letter code,
have the version number seperated by "_" rather than "." (example: 1.0 is 1_0), and then it will follow with the program name as shown above. So if we were to run gene blast on Qulo version 1.0 the output would
be Qulo.1_0.gene_blast in the gene index folder. More information will be given on the scripts and folder workflow in the following sections. 

<a name="next"></a>
## Workflow Sructure

The workflow to manage our data and the scripts we run on it are currently divided into several different bash scripts ased on each task required for the genome folder located in out [ftp_scripts](https://gitlab.com/TreeGenes/New_Genome_Pipeline/-/tree/master/ftp_scripts) repository.

![Nextflow Workflow](ftp_scripts/workflow.PNG "Workflow")

To run the necessary programs, the whole genome fasta, protein fasta, cds fasta, transcriptome file, and code (the 4 letter code and version number as for example Abal.1_0) in order to run the program. The
workflow is designed so that the whole genome fasta and code are required to run any operation wheras the protein fasta is only needed for protein indexes, cds fasta is needed for gene indexes, and transcriptome file for gmap after gmap build with the genome indexes.
As shown in the diagram above there are several checks on if the genomic data is present that is necessary to run the program, checks on the ouput to 
make sure it ran correctly, and checks on genome size to see which mode of gmap to run. Each script and its commands are explained in the following sections. 
As shown in the diagram, it is seperated into 6 different bash scripts based on each folder they run in. Each script is ran inside the 
main genome folder version.


<a name="genome"></a>
## Genome Folder

The genome folder should contain only 2 items, the whole genome fasta zipped and a README file containing the BUSCO and QUAST results. 
For this folder we need to run the [genome_folder_stats.sh](https://gitlab.com/TreeGenes/New_Genome_Pipeline/-/blob/master/ftp_scripts/genome_folder_stats.sh) 
script. This script only has one requirement which is the whole genome fasta unzipped and it needs to be run in the main genome folder.

It is executed by running the following command:

```
sbatch genome_folder_stats.sh
```

After doing so, a batch job is submitted and QUAST and BUSCO are run on the whole genome fasta and the results are outputted into a README that
gets stored in the genome folder and all other outputs are removed. 

<a name="index"></a>
## Index Folder

As mentioned before the index folder is divided into 3 sub folders: the genome index, the gene index and the protein index folders. 

This script will create individual folders for each program output and the folder should look like the following afterwards:

```
└── index
   ├── gene_index
   |    └── Qulo.1_0.gene_blast
   ├── genome_index
   |    ├── Qulo.1_0.bwa
   |    ├── Qulo.1_0.genome_blast
   |    ├── Qulo.1_0.gmap
   │    |    ├── Qulo.1_0.hisat2
   │    |    └── Qulo.1_0.hisat2_annotation
   └── protein_index
   |    ├── Qulo.1_0.protein_blast
   |    └── Qulo.1_0.protein_diamond
```

The next 4 sections explain the 4 different scripts and how to run them. 

<a name="genome_index"></a>
### Genome Index Folder

In the genome index folder we run 4 things: genome blast, bwa, gmap_build, and hisat2 which are all executed within the [genome_index.sh](https://gitlab.com/TreeGenes/New_Genome_Pipeline/-/blob/master/ftp_scripts/genome_index.sh)
script. This script has 2 required inputs, the whole genome fasta and the code/prefix for the folder. For example, the code for a genome within Abal version 1.0 
would be Abal.1_0. 

One would exeute it on the Qulo.1_0 genome by executing the following unix command:

```
sbatch genome_index.sh ./genome/Qulo.1_0.fa Qulo.1_0
```

and it would output the following:

This script will create individual folders for each program output and the folder should look like the following afterwards:

```
└── index
   ├── genome_index
   |    ├── Qulo.1_0.bwa
   |    ├── Qulo.1_0.genome_blast
   |    ├── Qulo.1_0.gmap
   |    └── Qulo.1_0.hisat
```
<a name="hisat2_annotation_index"></a>
### Hisat2 index with annotation
This index is created separately from the other indexes as it requires a GTF file and is slower to run than the other indexes. This script is under development and can be found [here](https://gitlab.com/TreeGenes/New_Genome_Pipeline/-/blob/master/ftp_scripts/hisat2_annotation.sh). 

The script is run with the following command:
```
sbatch hisat2_annotaion.sh ./genome/Qulo.1_0.fa ./annotation/Qulo.1_0.gtf Qulo.1_0
```

Todo: add gFACs check (do fasta counts agree with annotation), convert GFFs to GTF as necessary. 


<a name="gene_index"></a>
### Gene Index Folder

In the gene index folder we execute the script [gene_index.sh](https://gitlab.com/TreeGenes/New_Genome_Pipeline/-/blob/master/ftp_scripts/gene_index.sh)
which runs gene blast index. Like before it has 2 required inputs but this time its the cds fasta and the code instead of the 
whole genome fasta. 

It is run by executing the following:

```
sbatch gene_index.sh ./annotation/Qulo.1_0.cds.fa Qulo.1_0
```

And should ouput the following:

```
└── index
   ├── gene_index
   |    └── Qulo.1_0.gene_blast
```

<a name="prot"></a>
### Protein Index Folder

In the protein index folder we execute the script [protein_index.sh](https://gitlab.com/TreeGenes/New_Genome_Pipeline/-/blob/master/ftp_scripts/genome_protein_index.sh)
which runs protein BLAST and protein diamond. Like with the previous scripts it has 2 inputs; the protein fasta and the code.

It is run by executing the following:

```
sbatch protein_index.sh ./annotation/Qulo.1_0.pep.fa Qulo.1_0
```

And it should output the following:
```
└── index
   └── protein_index
   |    ├── Qulo.1_0.protein_blast
   |    └── Qulo.1_0.protein_diamond
```
<a name="ann"></a>
## Annotation Folder

The annotation folder should contain the cds fasta file and protein fasta (pep.fa) initially. Note that some of the versions/projects do not generate these files so
they may or may not contain one or the other of the pep or cds fastas. The programs we need to run on it 
for this folder are EnTAP, rnaQUAST, and BUSCO and the outputed files that will stay in this folder are the Entap final annotation lvl0 file,
entap outputed stastics tsv file, and a README.txt file with the rnaQUAST and BUSCO results. In this case we use seperate bash scripts as entap only
needs a pep.fa file wheras rnaQUAST needs cds and BUSCO needs pep.fa so the readme script would need both information.


<a name="en"></a>
### EnTAP

For the entap script [entap.sh](https://gitlab.com/TreeGenes/New_Genome_Pipeline/-/blob/master/ftp_scripts/entap.sh) the two required parameters are
the pep.fa file and the code for the genome. Note: we want to run EnTAP with the taxon information which must be set manually at the moment. Be sure to edit the file with the correct taxon information for the species before running.

The script is executed by running the following command:

```
sbatch entap.sh ./annotation/Qulo.1_0.pep.fa Qulo.1_0
```

And the output should be the following:

```
├──annotation
 ├── Qulo.1_0.entap_annotations.tsv
 ├── Qulo.1_0.entap_statistics.txt
```

the entap results directory is the other data from the run that can be accessed for reference. 

Todo: have the script grab the family name and supply for the taxon specification.


<a name="rna"></a>
### rnaQUAST and BUSCO

As explained before, rnaQUAST is run on the cds fasta and BUSCO is run in protein mode on the protein fasta. Since the output is just a README,
no code or prefix is necessary and the only required inputs are the cds fasta and protein fasta. For this we execute the [rnaquast_busco_stats.sh](https://gitlab.com/TreeGenes/New_Genome_Pipeline/-/blob/master/ftp_scripts/rnaquast_busco_stats.sh)
script. 

We execute the script with the following command:

```
sbatch rnaquast_busco_stats.sh ./annotation/Qulo.cds.fa ./annotation/Qulo.pep.fa
```

and the output should be a README in the annotation folder with the statistics. 

<a name="ali"></a>
## Alignments Folder

The alignments folder should contain the transcriptome file (if available) and the output gff3 file from gmap and README with gFACs output statistics.

<a name="gmap"></a>
## GMAP and gFACs

Gmap runs with the output from gmap_build and the transcriptome file in order to align the genome and output a gff3 file. In order to 
do this we run the [alignments.sh](https://gitlab.com/TreeGenes/New_Genome_Pipeline/-/blob/master/ftp_scripts/alignments.sh)
script which requires the whole genome fasta, the transcriptome file and the code for the genome as inputs. It also references the gmap_build output
from the genome indexes but that should already be completed from the genome index script since the only required input is the whole genome fasta.
So this script MUST be run after the genome_index script has been completed as indicated in the above workflow. 

gFACs runs after gmap is completed on the output gff3 file. It outputs a statistic file on the gene sturcutre that is used used as the main outputed
README file in the folder. The other results of gfacs are stored in the folder as well. 

The script is executed with the following command:

```
sbatch alignments.sh ./genome/Quro.1_0.fa ./alignments/Quro.1_0.tsa.fa Quro.1_0
```

and outputs the following:

```
├──alignments
 ├── Qulo.1_0.gmap.gff
 ├── Qulo.1_0.tsa.fa - transcript fasta used for gmap run
 ├── Qulo.1_0.gfacs_output/
 └── README.txt - summary of gmap results from gFACS
 
```

<a name="plastid"></a>
## Plastid
Plastid genomes are currently waiting to be brought in. When they are added they will include a README with stats from Quast (essentially the same as the genome readme script without BUSCO).
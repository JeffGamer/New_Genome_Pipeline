#!/bin/bash
#SBATCH --job-name=entNE
#SBATCH --mail-user=esgrau@gmail.com
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 25
#SBATCH --mem=30G
#SBATCH -o entap_%j.out
#SBATCH -e entap_%j.err
#SBATCH --partition=himem
#SBATCH --qos=himem

##NOTE: We want to run entap with the taxon information. This is not currently handled by the script so the taxon must be manually set for each species before running the script. In the entap command below 'Taxon' should be removed and replaced with the actual taxon.

if [ "$#" -ne 2 ]; then
    echo "Incorrect number of arguments" #input pep.fa file

else
    pfasta=$1
    code=$2

    module load anaconda/4.4.0
    module load perl/5.28.1
    module load diamond/0.9.25
    module load eggnog-mapper/0.99.1
    module load interproscan/5.25-64.0

    
 #trim fasta header if necessary (diamond doesn't like tabs in the header line)
    cut -d '    ' -f1 $pfasta > "${code}.filtered.pep.fa"

    /labs/Wegrzyn/EnTAP/EnTAP_v0.10.4/EnTAP/EnTAP --runP  -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.200.dmnd  -i "${code}.filtered.pep.fa" --out-dir "/home/FCAM/egrau/gmap/${species}/entap_results" --threads 12

    mv ./entap_results/log* ./annotation/"${code}.entap_statistics.txt"
    mv ./entap_results/final_results/final_annotations_lvl1.tsv ./annotation/"${code}.entap_annotations.tsv"
    rm "${code}.filtered.pep.fa"


fi

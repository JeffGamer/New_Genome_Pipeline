#!/bin/bash

# Usage: genome_indexer.sh directoryName target_nucletotide.fa target_protein.fa speciesCode
#        Will create a directory for the .fa file containing a directory for
#        each type of index
#        Run in the /index/ level of the genome directory
# Example:
#    sh generateIndex.sh EgrandisCDS Egrandis_162_cds.fa Egrandis_162_pep.fa EUGR

if [ "$#" -ne 4 ]; then
    echo "Incorrect number of arguments"
    echo "Usage: generateIndex.sh directoryName target_nucletotide.fa target_protein.fa speciesCode"
    echo "Usage example: sh generateIndex.sh EgrandisCDS Egrandis_162_cds.fa Egrandis_162_pep.fa EUGR"
else
    dir=$1
    fasta=$2
    pfasta=$3
    code=$4
    indexDir="Genome_Indexes"
    blast_fold="${code}_BLAST_gene"
    dmnd_fold="${code}_dmndBlast"

    mkdir "$indexDir"
    mkdir "$blast_fold"
    mkdir "$dmnd_fold"

    #Genome_indexes
    mkdir "${indexDir}/${code}_HISAT2_${dir}"
    mkdir "${indexDir}/${code}_GMAP_${dir}"
    mkdir "${indexDir}/${code}_BOWTIE2_${dir}"
    mkdir "${indexDir}/${code}_BWA_${dir}"

    #dmndBlast folders
    mkdir "${dmnd_fold}/${code}_ncbi_blast"
    mkdir "${dmnd_fold}/${code}_diamond"

    # hisat2
    module load hisat2
    hisat2-build --large-index $fasta -p 12 ${code} "${indexDir}/${code}_HISAT2_${dir}/${dir}"

    # bwa
    module load bwa
    bwa index -p "${indexDir}/${code}_BWA_${dir}/${dir}" $fasta

    # bowtie2
    module load bowtie2
    bowtie2-build -f $fasta "${indexDir}/${code}_BOWTIE2_${dir}/${dir}"

    # gmap section
    module load gmap
    gmap_build -D "${indexDir}/${code}_GMAP_${dir}/gmap" -d "${dir}" $fasta

    #nucleotide blast
    # blast
    # does not require proteins
    module load blast
    makeblastdb -in ${fasta} -parse_seqids -dbtype nucl -out "${blast_fold}/${code}"

    #protein blast + diamond
    #diamond
    #requires PROTEIN fasta
    module load diamond
    diamond makedb --in $pfasta -d $code
    mv $code.dmnd "${dmnd_fold}/${code}_diamond/"

    # blast for protein
    makeblastdb -in $pfasta -parse_seqids -dbtype prot -out "${dmnd_fold}/${code}_ncbi_blast/"
fi
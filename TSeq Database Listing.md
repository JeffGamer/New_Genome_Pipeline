TSeq (Tripal Sequence Similarity Search) provides users a list of available pre-indexed protein and nucleotide databases to run their DIAMOND or BLAST queries against.

Once the indexing stage is complete (see README.md), the database should be added to TSeq.
This can be done one of two ways:
1.  Filling out the form manually at `admin/tripal/extension/tseq/add_db`
2.  Importing a CSV sheet with 1 or more databases listed.

Required information is:
1. Type of database (Protein, Nucleotide, etc.)
2. Name of databae (human readable, will show up in a dropdown list)
3. Version of database
4. Category (Most likely 'Standard', see TSeq documentation if using additional categories)
5. File location (Must be accessible on disk to the user who is running the command. Note that
        this refers to the user that Tripal Remote Job logs into as, assuming the job is being run on a remote machine)
6. Web Location (Optional) - If the original non-indexed file is available for download, list it here
7. Count - the number of sequences/scaffolds held in the database. This can be found by running `grep ">" file.fa | wc -l` on the original sequence file
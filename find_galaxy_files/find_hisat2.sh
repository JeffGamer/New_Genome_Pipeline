#!/usr/bin/env bash

# uncomment below grep to re-fetch names file
#grep " -- " /isg/treegenes/treegenes_store/FTP/Genomes/README.txt | sort > names.txt
mapfile -t genomesArray < genome_hisat2.txt 
mapfile -t namesArray < names.txt
len=${#genomesArray[@]}

for ((i=0;i<len;i++))
do
	#echo "${namesArray[$i]} ${genomesArray[$i]}"
	genome=${genomesArray[$i]}
	#out=$(python namegetter.py "${namesArray[$i]}" "$genome")
	echo ${namesArray[$i]} | awk -v var="$genome" '{print $1"\t"$1"\t"$3" "$4"\t"var}'
       #	>> "/isg/treegenes/treegenes_store/GalaxyTGHWG/galaxy/tool-data/hisat2_indexes.loc"
	
done
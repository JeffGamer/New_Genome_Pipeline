#!/usr/bin/env bash

# This script attempts to locate all reference annotations from the TreeGenes FTP
# directory and add them to the refanninput.loc file used by Galaxy to present
# the user with a list of Reference Annotations that are available.
#
# It references the 'genome_fasta.txt' file, located within the FTP parent directory
# for a meaningful list of current directories in which to look for Reference Annotations
# Once it updates the refanninput.loc, it attempts to call the Galaxy API in
# order to update the listing.
# It also will warn the user that any Galaxy workflows which feature this .loc file
# will need to be updated in the Tripal Galaxy interface (delete and re-add).


# Assemble paths to input and output files
inNamesList='/isg/treegenes/treegenes_store/FTP/Genomes/README.txt'
inGenomeList='/isg/treegenes/treegenes_store/FTP/.current_versions/genome_fasta.txt'
outLocation='/isg/treegenes/treegenes_store/GalaxyTGHWG/galaxy/tool-data/AA_reference_annotation.loc'
galaxy_admin_api_key='' # Galaxy Admin API Key


# Define the export function to be used later.
# @todo Decide if this script should just echo a bunch of lines for the .loc or
#   whether it should try to replace the line in the file for each.
# args
#   $1 species code (Abal)
#   $2 Species and genus (Abies alba)
#   $3 /path/on/disk of listed file

function update_loc_file() {
    echo -e "$1\t$1\t$2\t$3"
}


# Get a list of all current Genomes available on the FTP
grep " -- " $inNamesList | sort > names.txt
mapfile -t namesArray < names.txt

# Get a list of all current genome directories available on the FTP
#  up to the version column
cut -d "/" -f 1-8 $inGenomeList > current_versions.txt
mapfile -t genomesArray < current_versions.txt

# Now we have a list of current version directories (/path/to/genome/species/version)
# We need to traverse each directory to see if a reference annotation exists, using
# the following pattern: 
# <current version path> / annotation / <code>.<converted version string>.gff.gz
# Example:
# /isg/treegenes/treegenes_store/FTP/Genomes/Frex/v0.5/annotation/Frex.0_5.gff.gz
len=${#genomesArray[@]}
#echo "Length: $len"
for ((i=0;i<len;i++))
do
    # Get the variables for the path
    genomePath=${genomesArray[$i]}
    speciesCode=$(cut -d "/" -f 7 <<< ${genomesArray[$i]})
    version=$(cut -d "/" -f 8 <<< ${genomesArray[$i]})

    # Convert the Genome version string to the filename version
    # by stripping out the leading 'v', replacing . with _, and preserving
    # any suffix (v1.0->1_0, v1.0b->1_0b)
    convertVersion=$(sed 's/v//' <<< $version | sed 's/[.]/_/')

    fileToTest=("$genomePath/annotation/$speciesCode.$convertVersion.gff.gz")

    if ! [[ -r $fileToTest ]]
    then
        # Test if there is an uncompressed version here
        fileToTest=("$genomePath/annotation/$speciesCode.$convertVersion.gff")
        if [[ -r $fileToTest ]]
        then
            # Get the 'Species genome' info from inNamesList (Genomes/README.txt)
            species=$(grep "$speciesCode --" names.txt | cut -b 10-)
            
        # There is some other reason we shouldn't add it to Galaxy
        #else
        #    echo "   ERROR with $fileToTest. Not including."
        fi
    else
        species=$(grep "$speciesCode --" names.txt | cut -b 10-)
        #echo -e "$speciesCode:\t$species" # replaced by function update_loc_file()
    fi
    update_loc_file "$speciesCode" "$species" "$fileToTest"
done

#mapfile -t annotationsArray < $inRefanList
#len=${#annotationsArray[@]}
#echo $len
# Loop through list of annotations and generate each entry for the
# refanninput.loc file.
# Columns are: <unique_build_id>  <dbkey> <display_name>  <file_path>
# Columns are tab separated (NOT SPACES)
#for ((i=0;i<len;i++))
#do
#    echo ${namesArray[$i]}
#done


# Update Galaxy by calling the API. -s -S for no progress but DO show errors
curl -s -S "https://treegenes.cam.uchc.edu/api/tool_data/AA_reference_annotation/reload?key=$galaxy_admin_api_key" > /dev/null

# Update Tripal Galaxy

#!/usr/bin/env bash

# Instructions:
# Run the below find -name "*.fa*
# Manually check genome_fasta.txt for any out of date genomes, or fasta files we don't want.
# Uncomment the loop below- it's commented for safety because it modifies all_fasta.loc.
# Then run this script.

# uncomment below to re-fetch genomes list
# find /isg/treegenes/treegenes_store/FTP/Genomes -name "*.fa*" | grep -vE 'annotation|supplemental|index|alignments|extracted_proteins|entap_result|en_result|ontology|transcriptome|busco_out|masked|scaffold|hisat|idx' |sort > genome_fasta.txt

grep " -- " /isg/treegenes/treegenes_store/FTP/Genomes/README.txt | sort > names.txt
mapfile -t genomesArray < genome_fasta.txt
mapfile -t namesArray < names.txt
len=${#genomesArray[@]}

for ((i=0;i<len;i++))
do
        genome=${genomesArray[$i]}
        echo ${namesArray[$i]} | awk -v var="$genome" '{print $1"\t"$1"\t"$3" "$4"\t"var}' >> "/isg/treegenes/treegenes_store/GalaxyTGHWG/galaxy/tool-data/all_fasta.loc"
done
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

def main():

    parser = argparse.ArgumentParser(description='Takes in line, returns first word in the string.')

    parser.add_argument('LINE');
    parser.add_argument('GENOME');
    args = parser.parse_args();

    org_name = args.LINE.split()[0];
    genome = args.GENOME;
    #print("org_name = {}\n".format(org_name));
    #print("genome = {}\n".format(genome));
    if org_name in genome:
        print "success!\n";
        return org_name + " " + genome;
    else:
        print('fail\n\n')
        return 'fail';

if __name__ == "__main__":

    main()

UniGenes FTP directory structure:
```
UniGenes
│
└── Qulo
    ├──v1
    │    ├── 70
    │    |    ├── README.txt
    │    |    ├── 1_70_UniGenes_Qulo_annotations.tsv
    │    |    ├── 1_70_UniGenes_Qulo_complete_partial_prots.fa
    │    |    └── 1_70_UniGenes_Qulo_complete_partial_genes.fa
    │    └── 98
    │         ├── README.txt
    │         ├── 1_98_UniGenes_Qulo_annotations.tsv
    │         ├── 1_98_UniGenes_Qulo_complete_partial_prots.fa
    │         └── 1_98_UniGenes_Qulo_complete_partial_genes.fa
    └── v2
         ├── 70
         |    ├── README.txt
         |    ├── 2_70_UniGenes_Qulo_annotations.tsv
         |    ├── 2_70_UniGenes_Qulo_complete_partial_prots.fa
         |    └── 2_70_UniGenes_Qulo_complete_partial_genes.fa
         └── 98
              ├── README.txt
              ├── 2_98_UniGenes_Qulo_annotations.tsv
              ├── 2_98_UniGenes_Qulo_complete_partial_prots.fa
              └── 2_98_UniGenes_Qulo_complete_partial_genes.fa

```


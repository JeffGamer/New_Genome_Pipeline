#!/bin/bash
#SBATCH --job-name=genome_indexer
#SBATCH -n 12
#SBATCH --mem=96G
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mail-user=your.email@www.com
#SBATCH -o index.out
#SBATCH -e index.err

sh genome_indexer.sh speciesName target_nucletotide.fa target_protein.fa speciesCode
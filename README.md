A general pipeline/how-to for converting annotated genome files to more usable formats.

Currently, in the TreeGenes database, Genomes are located within the FTP folder at:  
`/isg/treegenes/treegenes_store/FTP/Genomes`

Species are organized by four letter species code names, which are typically the first two letters of the species name combined with the first two letters of the genus name. For example, _Betula nana_ will become `Bena`. In some cases, if the four letter code is already in use by another species, the new species' code will become the first two letters of the genus name, combined with the second and third letters of the species name. In this case, _Abies balsamea_ becomes `Abal` since `Abba` is already taken. 

In order to keep track of these codes, there is an entry in the `organismprop` table for each species. Example query below:
```
select o.genus "Genus", o.species "Species", op.value "Code" from chado.organism o 
 join chado.organismprop op on o.organism_id=op.organism_id
 where op.type_id=52307;

      Genus      |    Species    | Code 
-----------------+---------------+-------
 Calocedrus      | decurrens     | Cade
 Cryptomeria     | japonica      | Crja
 Larix           | decidua       | Lade
 Larix           | kaempferi     | Laka
 ...
```



The following steps are covered:  
1. indexing the genomes (hisat2, bwa, bowtie2, gmap, last, diamond)
2. preparing and loading files for JBrowse
3. preparing and loading files for TSeq (Diamond and BLAST indexes only)
4. Updating Galaxy config files for Tools

For the indexing, a simple shell script will load all of the necessary tools and use them on the nucleotide and protein files. Tool commands may vary based on version so it is best to double check error and output files.

In the `Genomes` folder, assuming a 4 letter code of `Qulo` for an organism, the directory hierarchy will look similar to this, after the indexing has completed:
```
An example of file structure and naming conventions using Quercus lobata (species code Qulo) genome version 1.0

Qulo
│
└── v1.0
    ├──README.txt (optional) - Contains information about the genome project including publication/attribution. If genome was size selected and/or includes masked and unmasked scaffolds it will be noted here.
    ├──alignments (optional)
    │    ├── Qulo.1_0.gmap.gff
    │    ├── Qulo.1_0.tsa.fa - transcript fasta used for gmap run
    │    └── README.txt - summary of gmap results from gFACS
    ├──annotation
    │    ├── Qulo.1_0.gff.gz
    │    ├── Qulo.1_0.pep.fa.gz
    │    ├── Qulo.1_0.repeats.fa.gz (optional)
    │    ├── Qulo.1_0.repeats.gff.gz (optional)
    │    ├── Qulo.1_0.entap_annotations.tsv
    │    ├── Qulo.1_0.entap_statistics.txt
    │    └── README.txt - rnaQUAST and BUSCO results
    ├── genome
    │    ├── Qulo.1_0.fa.gz
    │    └── README.txt - QUAST and BUSCO results
    ├──  index
    │    ├── gene_index
    │    |    └── Qulo.1_0.gene_blast
    │    ├── genome_index
    │    |    ├── Qulo.1_0.bwa
    │    |    ├── Qulo.1_0.genome_blast
    │    |    ├── Qulo.1_0.gmap
    │    |    ├── Qulo.1_0.hisat2
    │    |    └── Qulo.1_0.hisat2_annotation
    │    └── protein_index
    │         ├── Qulo.1_0.protein_blast
    │         └── Qulo.1_0.protein_diamond
    ├── plastid (optional)
    │   ├── mitochondrial
    │   │    ├── Qulo.1_0.mito.gff.gz
    │   │    ├── Qulo.1_0.mito.fa.gz
    │   │    └── README.txt 
    │   └── chloroplast
    │        ├── Qulo.1_0.chloro.gff.gz
    │        ├── Qulo.1_0.chloro.fa.gz
    │        └── README.txt
    └──  supplemental (optional)
        ├── Qulo.1_0.annotations.tsv.gz (If original project provides annotation files they will be located in the supplemental directory. In-house annotations are located in the annotation directory.)
        ├── Qulo.1_0.snps.vcf.gz 
        └── Qulo.1_0.polymorphisms.vcf.gz

```


## Galaxy
Tools within Galaxy need to be able to access these files. Data files are added on a tool-by-tool basis. This is a multi-step process involving multiple config files.

All files below are relative to the root Galaxy directory: `/isg/treegenes/treegenes_store/Galaxy/Galaxy/`

The **BLAST** tool will be used as an example for the following list of files:
 1. Main config: `config/tool_data_table_conf.xml`
    - This is the master list of all file lists. An entry for each Specific Config (2) needs to be made:
    ```
    <!-- Locations of nucleotide BLAST databases -->
    <table name="blastdb" comment_char="#" allow_duplicate_entries="False">
        <columns>value, name, path</columns>
        <file path="tool-data/blastdb.loc" />
    </table>
    ```
 2. Specific config: `tool_data/blastdb.loc`
    - This file contains a listing of all files and their disk location:
    ```
    # This is a sample file distributed with Galaxy that is used to define a
    # list of nucleotide BLAST databases, using three columns tab separated:
    #
    # <unique_id>{tab}<database_caption>{tab}<base_name_path>
    #
    Capa_Gene_v0.4  Carica papaya Gene, v0.4        /isg/treegenes/treegenes_store/FTP/Genomes/Capa/v0.4/index/CAPA_BLAST_gene/Capa.0_4.c$
    Elgu_Gene_v1.0  Elaeis guineensis Gene, v1.0    /isg/treegenes/treegenes_store/FTP/Genomes/Elgu/v1.0/index/ELGU_BLAST_gene/Elgu.1_0.c$
    Euca_Gene_v1.0  Eucalyptus camaldulensis Gene, v1.0     /isg/treegenes/treegenes_store/FTP/Genomes/Euca/v1.0/index/EUCA_BLAST_gene/Eu$
    ```
 3. Tool file
    - Each tool will know what file it requires but may not be able to find them.

 4. Updating Galaxy Location files
    - Once files are added to their respective `.loc` file, Galaxy must be updated. This can be accomplished by making a call to the API: ([more information](https://docs.galaxyproject.org/en/master/api/api.html#module-galaxy.webapps.galaxy.api.tool_data))
    ```https://treegenes.cam.uchc.edu/api/tool_data/<tool_name>/reload```
      - tool_name can be found by calling just `api/tool_data`.
    - Calling this API lets us avoid restarting Galaxy.

## File Permissions
All files in the FTP directory, for the purposes of reachability from nginx (aka the website) and to be able to be modified by lab members, **must** have the following permissions:

 - Group: `treegenes`
 - Permissions: `775` or `drwxrwxr-x` (directories) or `rwxrwxr-x` (regular files)

The file owner can be whoever, although it is preferred to be `tg-nginx`.

To set these permissions:
  - For files: `chgrp treegenes filename` & `chmod 775 filename`
  - For directories (recursive) `chgrp -R treegenes directory/` & `chmod -R 775 directory/`

If you're not sure which files you own,  you can get a list:
`find /isg/treegenes/treegenes_store/FTP/ -user $USER@cam.uchc.edu`
For more detailed information on the files you own:
`find /isg/treegenes/treegenes_store/FTP/ -user $USER@cam.uchc.edu -exec stat -c '%a %G %n' {} \;`

